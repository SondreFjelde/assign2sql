INSERT INTO SUPERHERO (Name, Alias, Origin)
VALUES ('Superman', 'Clark Kent', 'Krypton');

INSERT INTO SUPERHERO (Name, Alias, Origin)
VALUES ('Batman', 'Bruce Wayne', 'Gotham City');

INSERT INTO SUPERHERO (Name, Alias, Origin)
VALUES ('Spiderman', 'Peter Parker', 'New York');