namespace SQLCoding
{
    public interface ICustomer
    {
        public bool GetAllCustomers(Customer newCustomer);
        public bool AddCustomer(Customer customer);

        public bool DeleteCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer);



    }
}
